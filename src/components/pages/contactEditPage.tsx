import { contactsClient } from '@lib/clients/contacts';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { IPerson } from '@lib/models/person';
import TextField from '@mui/material/TextField';

const fetchUser = async (id: string) => contactsClient.getContactById(id);

export const ContactEditPage = () => {
  const { id } = useParams();
  const [user, setUser] = useState<IPerson>(null);

  useEffect(() => {
    async function getUser() {
      const fetchedUser = await fetchUser(id);
      setUser(fetchedUser);
    }
    getUser();
  }, [id]);

  const handleSubmit = async (e: React.FormEventHandler<HTMLFormElement>) => {
    e.preventDefault();
    console.log(e);
  };

  return (
    <form onSubmit={handleSubmit}>
      <TextField
        required
        id="outlined-required"
        label="Required"
        defaultValue="Hello World"
      />
    </form>
  );

  return <p>{JSON.stringify(user)}</p>;
};
